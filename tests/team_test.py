import numpy as np
import sys
import os

current = os.path.dirname(os.path.realpath(__file__))
parent = os.path.dirname(current)
sys.path.append(parent)

from resources.team import Team as team

TEAMS = [
        'Jagielonia Bialystok',
        'Wisla Krakow', 
        'Lech Poznan']


def test_add_team():
    teams_added = ['Slask Wroclaw', 'Zaglebie Lubin',]
    teams = team(TEAMS)
    teams = team(teams_added)
    TEAMS.append(teams_added)
    if np.array_equal(np.array(teams.return_teams), TEAMS):
        pass


def test_return_teams():
    teams = team(TEAMS)
    if np.array_equal(np.array(teams.return_teams()), TEAMS):
        pass


def test_count_fixtures():
    teams = team(TEAMS)
    if teams.count_fixtures == 3:
        pass
