from resources.team import Team
import numpy as np

if __name__ == "__main__":
    teams = [
        'Jagielonia Bialystok', 
        'Wisla Krakow', 
        'Lech Poznan', 
        'Pogon Szczecin', 
        'Rakow Czestochowa', 
        'Lechia Gdansk',
        'Radomiak Radom',
        'Wisla Plock',
        'Piast Gliwice',
        'Gornik Zabrze',
        'Legia Warszawa',
        'Cracovia Krakow',
        'Warta Poznan',
        'Stal Mielec',
        'Slask Wroclaw',
        'Zaglebie Lubin',
        'Termalica Bruk-Bet Nieciecza',
        'Gornik Leczna']

    t = Team(teams)
    print(t.return_teams())
    print(t.count_fixtures())
