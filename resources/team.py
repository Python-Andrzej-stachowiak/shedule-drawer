class Team:
    def __init__(self, teams):
        self.team_list = []
        self.add_team(teams)

    def add_team(self, teams):
        for team in teams:
            self.team_list.append(team)

    def return_teams(self):
        return self.team_list

    def count_fixtures(self):
        return len(self.team_list)
